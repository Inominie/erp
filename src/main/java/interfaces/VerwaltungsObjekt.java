package interfaces;
/**
 * 
 * Abstrakte Klasse fuer alle Verwaltungs Objekte.<br><br>
 * Wird genutzt um allen Objekten eine eindeutige ID zuzuweisen und um in der {@link verwaltungs_Objekte.buchungen.Buchung Buchung} generisch auf die verschiedenen
 * VerwaltungsObjekte zuzugriefen.
 *
 */
public abstract class VerwaltungsObjekt {
	public int ID;
	
	protected VerwaltungsObjekt(int id){
		ID = id;
	}
}
