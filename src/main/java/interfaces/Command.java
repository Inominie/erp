package interfaces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import erp_software.Erp;
/**
 * Command Interface fuer die verschiedenen Command Handler.<br><br>
 *
 */
public interface Command {

	/**
	 * Methode zum Start der Kommando Eingabe.
	 */
	public abstract void startScan();
	
	/**
	 * Default Methode die aus der Eingabe des Users eine mitgegebenen ID filtert.
	 * @param command Eingabe des Users
	 * @return ID als Integer
	 */
	public default int getCommandID(String command){
		Pattern pattern = Pattern.compile("\\[(.*?)\\]");
        Matcher match = pattern.matcher(command);

        if (match.find() && match.group(1) != null) {
            return Integer.parseInt(match.group(1));
        } else {
            System.out.println("Fehlerhafte Befehlssyntax!");
            return 0;
        }
	}
	
	/**
	 * Default Methode zur Ausgabe der Config Datei der verschiedenen Command Handler.<br>
	 * @param caller Klassenname der Aufrufenden Klasse
	 */
	public default void displayConfig(String caller){
			InputStream res = Erp.class.getResourceAsStream("/" + caller + "Config");

	        BufferedReader reader = new BufferedReader(new InputStreamReader(res));
	        String line = null;
	        try {
	            while ((line = reader.readLine()) != null) {
	                System.out.println(line);
	            }

	            reader.close();
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	}
}
