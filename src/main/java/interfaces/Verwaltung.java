package interfaces;

/**
 * Interface zur Verwaltung
 * 
 *
 */
public interface Verwaltung {

	/**
	 * Methode zum Ausgeben der n�chsten ID 
	 * @return naechste ID
	 */
	public int getNextID();
}
