package verwaltungs_Objekte.it;

import java.util.ArrayList;
import java.util.List;

import erp_software.XMLHandler;
import interfaces.Verwaltung;

/**
 * Klasse zur Verwaltung der Geraete.<br>
 * Aus dieser Klasse wird die XML-Datenbank der Geraete erstellt im {@link erp_software.XMLHandler XMLHandler}.<br>
 * Au�erdem wird hier die ID der Geraete erstellt {@link #getNextID() getNextID}.
 *
 *@see Verwaltung
 *@see Device
 */
public class DeviceVerwaltung implements Verwaltung {

	private List<Device> devices = new ArrayList<>();

	/**
	 * Methode zum Ausgeben der aktuellen Geraeteliste.
	 * @return Geraete Liste
	 */
	public List<Device> getDevices() {
		return devices;
	}

	/**
	 * Methode zum Anlegen eines neuen Geraetes
	 * Die Methode erzeugt ein neues Geraet und fuegt dies mit Hilfe der Methode {@link #addDevice(Device)} an.
	 * @param name Geraetename
	 * @param type Geraetetyp
	 * @param preis Geraetepreis
	 * @param nutzung Nutzung
	 */
	public void createDevice(String name, DeviceType type, int preis, int nutzung) {
		Device d = new Device(name, type, preis, nutzung, getNextID());
		d.setIpv4(initIP());
		d.setMac(initMac());
		d.setSubnet("255.255.192.0");
		d.setGateway("255.255.255.0");
		addDevice(d);
	}

	/**
	 * Methode zum hinzufuegen eines neuen Geraetes
	 * Die Methode prueft ob das Geraet bereits vorhanden ist und updatet die XML-Datei nach dem Hinzufuegen des neuen Geraetes. 
	 * @param pDev Device Objekt
	 * @see XMLHandler#updateXML(Object)
	 */
	public void addDevice(Device pDev) {
		for (Device d : devices) {
			if (d.equals(pDev)) {
				System.out.println("Gerät bereits vorhanden!");
				break;
			}
		}
		devices.add(pDev);
		XMLHandler.updateXML(this);
		System.out.println("Geraet wurde hinzugefuegt.");

	}

	/**
	 * Methode zum Loeschen eines Geraetes
	 * Die Methode prueft ob das Geraet vorhanden ist, loescht dieses und updatet die XML-Datei.
	 * @param pDev Device Objekt
	 * @see XMLHandler#updateXML(Object)
	 */
	public void deleteDevice(Device pDev) {
		for (Device d : devices) {
			if (d.equals(pDev)) {
				devices.remove(pDev);
				XMLHandler.updateXML(this);
				System.out.println("Geraet wurde geloescht!");
				return;
			}	
		}
		System.out.println("Geraet konnte nicht geloescht werden!");
	}

	/**
	 * Methode zum Ausgeben eines Mitarbeiters anhand der ID.
	 * @param pID GeraeteID
	 * @return Device Objekt
	 */
	public Device getDevice(int pID) {
		for (Device d : devices) {
			if (d.getID() == pID) {
				return d;
			}
		}
		return null;
	}

	/**
	 * Methode zur generierung der naechsten ID.
	 * Die Methode liest die aktuell hoechste ID der Geraete und
	 * gibt die naechst hoehere zurueck.
	 * @return naechste verfuegbare ID
	 * @see Verwaltung
	 */
	public int getNextID() {
		int id = 0;
		for (Device d : devices) {
			if (d.getID() > id)
				id = d.getID();
		}
		id++;
		return id;
	}
	
	/**
	 * Methode zum Generieren einer IP
	 * @return generierte IP
	 */
	private String initIP(){
		String ipFix = "10.10.4.";
		return ipFix + getNextID();
	}
	
	/**
	 * Methode zum Generieren einer MAC
	 * @return generierte MAC
	 */
	private String initMac() {
		String macFix = "00:80:41:ae:fd:";
		if (getNextID() > 9){
			return macFix + getNextID();
		}
		else {
			return macFix + getNextID() + "e";
		}
	}

}
