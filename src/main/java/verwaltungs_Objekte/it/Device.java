package verwaltungs_Objekte.it;

import erp_software.XMLHandler;
import interfaces.VerwaltungsObjekt;
/**
 * Klasse der IP-Ger�te.<br>
 * Enth�lt relevante Verbindungsdate wie ipv4 oder MAC-Addresse und
 * Preis und Nutzungsdauer.<br>
 * Der Typ des Ger�tes f�r durch die ENUM-Klasse {@link DeviceType} definiert.
 *@see DeviceType
 *@see DeviceVerwaltung
 */
public class Device extends VerwaltungsObjekt {
	
	private String ipv4, subnet, gateway, mac, name;
	private double preis, nutzung;
	private final DeviceType type;
	/**
	 * Kontruktor zum anlegen eines neuen Ger�tes
	 * @param name Ger�tename
	 * @param type Ger�tetyp
	 * @param preis Anschaffungspreis
	 * @param nutzung Nutzungsdauer
	 * @param id eindeutige Identifikationsnummer
	 */
	public Device(String name, DeviceType type, double preis, double nutzung, int id) throws RuntimeException{
		super(id);
		if(checkEquality(name, type)){
		this.name = name;
		this.type = type;
		this.preis = preis;
		this.nutzung = nutzung;	
		}
		else {
			throw new RuntimeException("Geraet bereits vorhanden!");
		}
	}
	/**
	 * "Null"-Konstruktor zur Initialisierung im XML-Handler
	 * @see erp_software.XMLHandler
	 */
	public Device(){
		super(0);
		this.name = null;
		this.type = null;
		this.preis = 0;
		this.nutzung = 0;
	}
	
	public boolean equals(Device pDev){
		if (pDev.ID == 0 && ID == 0){
			return true;
		}else if (pDev.ID == 0 || ID == 0){
			return false;
		}
		if(this.name.equals(pDev.getName()) && 
				this.type.equals(pDev.getType()) &&
					this.ID == pDev.getID()){
			return true; //Device ist bereits vorhanden
		}
		return false; //Device gibt es noch nicht
	}
	public boolean checkEquality(String name, DeviceType type){
		DeviceVerwaltung da = (DeviceVerwaltung) (XMLHandler.getXML(new DeviceVerwaltung()));
		
		for (Device d : da.getDevices()){
			if ( d.ID == 0){
				continue;
			}
			if (d.getName().equals(name) &&
				d.getType().equals(type)){
				return false;
			}
		}
		return true;
	}
	/**
	 * Methode zur Ausgabe der allgemeinen Ger�teinformationen auf der Konsole.
	 * @return Ausgabetext
	 */
	public String toString(){
		return (this.name + "\n" + this.type + "\n" + this.ID);
	}
	/**
	 * Methode zur Ausgabe der technischen Ger�teinformationen auf der Konsole.
	 * @return Ausgabetext
	 */
	public String tecToString(){
		return (this.name + "\n" + this.type + "\nID:" + this.ID + "\nMAC:" + this.mac + "\nIP:" + this.ipv4 + "\nGateway:" + this.gateway + "\nSubnet:" + this.subnet);
	}
	
	public int getID(){
		return this.ID;
	}

	public String getIpv4() {
		return ipv4;
	}

	public void setIpv4(String ipv4) {
		this.ipv4 = ipv4;
	}

	public String getSubnet() {
		return subnet;
	}

	public void setSubnet(String subnet) {
		this.subnet = subnet;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public double getNutzung() {
		return nutzung;
	}

	public void setNutzung(double nutzung) {
		this.nutzung = nutzung;
	}

	public DeviceType getType() {
		return type;
	}
}
