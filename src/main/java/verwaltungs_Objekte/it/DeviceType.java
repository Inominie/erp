package verwaltungs_Objekte.it;
/**
 * ENUM-Klasse zur spezifizierung der einzelnen Gerätetypen
 *
 */
public enum DeviceType {
	SERVER, WORKSTATION, NOTEBOOK, HANDY, TABLET, DRUCKER, TELEFON, ROUTER, SWITCH, IOTDEVICE;
/**
 * Methode zur Ausgabe der verschiedenen Gerätetypen auf der Konsole.
 * @return Ausgabetext
 */
	public static String ausgabe(){
		return "SERVER, WORKSTATION, NOTEBOOK, HANDY, TABLET, DRUCKER, TELEFON, ROUTER, SWITCH, IOTDEVICE";
	}
}
