/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package verwaltungs_Objekte.human_resources;

import java.util.Scanner;

import erp_software.Pair;
import erp_software.XMLHandler;
import interfaces.VerwaltungsObjekt;

/**
 * Klasse die zur Speicherung von Mitarbeiter relevanten Daten dient<br>
 * Erweitert das VerwaltungsObjekt
 */
public class Mitarbeiter extends VerwaltungsObjekt {

	private Pair<String, String> login;
	private String vorname;
	private String nachname;
	private long telefon;
	private String email;
	private final String gebdatum;

	/**
	 * Konstruktor zum erzeugen eines neuen Mitarbeiters.
	 * @param pVorname Vorname
	 * @param pNachname Nachname
	 * @param pGebdatum Geburtsdatum
	 * @param id eindeutige Identifikationsnummer
	 *
	 */
	public Mitarbeiter(String pVorname, String pNachname, String pGebdatum, int id) throws RuntimeException {
		super(id);
		if (checkEquality(pVorname, pNachname, pGebdatum)) {
			vorname = pVorname;
			nachname = pNachname;
			gebdatum = pGebdatum;
			createLogin();
		} else {
			throw new RuntimeException("Mitarbeiter bereits vorhanden!");
		}

	}
	/**
	 * Default Konstruktor, der einen "null"-Mitarbeiter erstellt zur initialisierung der XML Dateien.
	 */
	public Mitarbeiter(){
		super(0);
        vorname = "";
        nachname = "";
        gebdatum = "";
        login = new Pair<>("Dummy", "dummy123");
    }

	public int getID() {
		return ID;
	}

	public void setLogin(Pair<String, String> login) {
		this.login = login;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public long getTelefon() {
		return telefon;
	}

	public void setTelefon(long telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGebdatum() {
		return gebdatum;
	}

	@Override
	/**
	 * Methode zur Ausgabe der Mitarbeiter relenvanten Daten auf der Konsole.
	 */
	public String toString() {
		String ausgabe = new String();
		ausgabe = (this.vorname + " " + this.nachname + "\nGeburtstag:" + this.gebdatum + "\nID:" + this.ID);
		if(telefon != 0)
			ausgabe += "\nTelefon: " + this.telefon;
		if(email != null)
			ausgabe += "\nEMail: " + this.email;
		return ausgabe;
	}

	/**
	 * Methode zum Vergleich von zwei Mitarbeiter-Objekten auf Gleichheit.
	 * @param pMit Mitarbeiter mit dem das aktuelle Objekt verglichen werden soll.
	 * @return true = identisch, flase = verschiedenen Objekte
	 */
	public boolean equals(Mitarbeiter pMit) {
		if (ID == 0 && pMit.ID == 0){
			return true;
		}
		else if (ID == 0 || pMit.ID == 0){
			return false;
		}
		return (this.getVorname() == null ? pMit.getVorname() == null : this.getVorname().equals(pMit.getVorname()))
				&& (this.getNachname() == null ? pMit.getNachname() == null
						: this.getNachname().equals(pMit.getNachname()))
				&& (this.getGebdatum() == null ? pMit.getGebdatum() == null
						: this.getGebdatum().equals(pMit.getGebdatum()));
	}

	/**
	 * Methode zum pruefen ob ein Mitarbeiter bereits in der Datenbank vorhanden ist.
	 * @param vName Vorname
	 * @param nName Nachname
	 * @param bDay Geburtstag
	 * @return true = Mitarbeiter nicht vorhanden, false = Mitarbeiter vorhanden.
	 */
	public boolean checkEquality(String vName, String nName, String bDay) {
		MitarbeiterVerwaltung maKartei = new MitarbeiterVerwaltung();
		maKartei = (MitarbeiterVerwaltung) XMLHandler.getXML(maKartei);

		for (Mitarbeiter m : maKartei.getMitarbeiter()) {
			if (m.ID == 0){
				continue;
			}
			if ((m.getVorname().equals(vName) && m.getNachname().equals(nName) && m.getGebdatum().equals(bDay)))
				return false;
		}
		return true;
	}

	/**
	 *  Erzeugt mit User-Eingabe ein Login fuer einen neu angelegten Mitarbeiter.
	 */
	private void createLogin() {
		Scanner sc = new Scanner(System.in);
		String user = null, pw = null;
		System.out.println("Login fuer Mitarbeiter: " + this.getVorname() + " " + this.getNachname());

		while (user == null || pw == null) {

			if (user == null && pw == null) {
				System.out.println("Bitte Benutzernamen angeben: \n");
				user = sc.next();
				while (!((MitarbeiterVerwaltung) (XMLHandler.getXML(new MitarbeiterVerwaltung()))).checkUser(user)) {
					System.out.println("Username bereits vergeben!");
					user = sc.next();
				}
			} else if (pw == null) {
				System.out.println("Bitte Passwort angeben: \n");
				pw = sc.next();
				while (pw.length() < 6) {
					System.out.println("Das Passwort muss mindestens 6-stellig sein!");
					pw = sc.next();
				}
			}
			login = new Pair<String, String>(user, pw);
		}
		sc.close();
	}

	public String getUser() {
		return login.getElement0();
	}

}
