/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package verwaltungs_Objekte.human_resources;

import java.util.ArrayList;
import java.util.List;

import erp_software.XMLHandler;
import interfaces.Verwaltung;
/**
 * Klasse zur Verwaltung der Mitarbeiter.<br>
 * Aus dieser Klasse wird die XML-Datenbank der Mitarbeiter erstellt im {@link erp_software.XMLHandler XMLHandler}.<br>
 * Au�erdem wird hier die ID der Mitarbeiter erstellt {@link #getNextID() getNextID}.
 *
 *@see Verwaltung
 *@see Mitarbeiter
 */
public class MitarbeiterVerwaltung implements Verwaltung {

	private List<Mitarbeiter> mitarbeiter = new ArrayList<>();
	/**
	 * Standard Konstruktor
	 */
	public MitarbeiterVerwaltung() {	
	}
	/**
	 * Methode zum anlegen eines neuen Mitarbeiters anhand von Input Daten.<br>
	 * Ruft die {@link #addMitarbeiter(Mitarbeiter) addMitarbeiter} Methode auf.
	 * @param pVorname Vorname
	 * @param pNachname Nachname
	 * @param pGebdatum Geburtsdatum
	 */
	public void createNewMitarbeiter(String pVorname, String pNachname, String pGebdatum) {
		addMitarbeiter(new Mitarbeiter(pVorname, pNachname, pGebdatum, getNextID()));
	}
	/**
	 * Methode zum hinzufuegen eines neuen Mitarbeiters.<br>
	 * Updatet die XML-Datenbank nach dem Hinzufuegen des neuen Mitarbeiters.
	 * @param pMit Mitarbeiter Objekt
	 * @see XMLHandler#updateXML(Object)
	 */
	public void addMitarbeiter(Mitarbeiter pMit) {
		for (Mitarbeiter m : getMitarbeiter()) {
			if (m.equals(pMit)) {
				System.out.println("Mitarbeiter bereits vorhanden!");
				break;
			}
		}
		getMitarbeiter().add(pMit);
		XMLHandler.updateXML(this);
		System.out.println("Mitarbeiter wurde hinzugefuegt.");
	}
	
	/**
	 * Methode zum loeschen eines Mitarbeiters.<br>
	 * Prueft ob der Mitarbeiter vorhanden ist, loescht ihn aus der ArrayList und updated die XML.
	 * @param pMit Mitarbeiter Objekt
	 * @return true = Mitarbeiter geloescht, false = Mitarbiter konnte nicht geloescht werden.
	 * @see XMLHandler#updateXML(Object)
	 */
	public boolean deleteMitarbeiter(Mitarbeiter pMit) {
		for (Mitarbeiter m : getMitarbeiter()) {
			if (m.equals(pMit)) {
				getMitarbeiter().remove(pMit);
				XMLHandler.updateXML(this);
				System.out.println("Mitarbeiter wurde geloescht!");
				return true;
			}
		}
		System.out.println("Der ausgewaehlte Mitarbeiter konnte leider nicht geloescht werden!");
		return false;
	}
	/**
	 * Methode die ein Mitarbeiter Objekt zurueck gibt anhand einer ID.
	 * @param pID ID des gewuenschten Mitarbeiters
	 * @return Mitarbeiter Objekt
	 */
	public Mitarbeiter getMitarbeiter(int pID) {
		for (Mitarbeiter m : getMitarbeiter()) {
			if (m.getID() == pID) {
				return m;
			}
		}
		return null;
	}
	/**
	 * Methode zur generierung der ID. Liest die aktuell hoechste verwendete ID der Mitarbeiter
	 * aus und gibt die naechst hoehere zurueck.
	 * @return neue ID als Integer
	 * @see Verwaltung
	 */
	public int getNextID() {
		int highID = 0;
		for (Mitarbeiter m : getMitarbeiter()) {
			if (m.getID() >= highID)
				highID = m.getID();
		}
		highID++;
		return highID;
	}
	/**
	 * Methode zur ueberpruefung ob ein User-Login bereits genutzt wird.<br>
	 * @param user Login der genutzt werden moechte
	 * @return true = Login noch verf�gbar, false = Login bereits vorhanden
	 */
	public boolean checkUser(String user) {

		for (Mitarbeiter m : getMitarbeiter()) {

			if (m.getUser().equals(user)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Methode die die aktuelle Mitarbeiter Liste zurueck gibt.
	 * @return Mitarbeiter Liste
	 */
	public List<Mitarbeiter> getMitarbeiter() {
		return mitarbeiter;
	}

}
