package verwaltungs_Objekte.buchungen;

import java.util.ArrayList;

import erp_software.XMLHandler;
import interfaces.Verwaltung;
import interfaces.VerwaltungsObjekt;

/**
 * Klasse zur Verwaltung der Buchungen.<br>
 * Aus dieser Klasse wird die XML-Datenbank der Buchungen erstellt im {@link erp_software.XMLHandler XMLHandler}.<br>
 * Au�erdem wird hier die ID der Buchung erstellt {@link #getNextID() getNextID}.
 * 
 * @see Verwaltung
 * @see Buchung
 */
public class BuchungsVerwaltung implements Verwaltung {

	private ArrayList<Buchung> buchungen = new ArrayList<>();
	/**
	 * Methode zum anlegen einer neuen Buchung.<br>
	 * Ruft die {@link #addBuchung(Buchung) addBuchung} Methode auf.
	 * @param e1 Buchendes VerwaltungsObjekt(Mitarbeiter)
	 * @param e2 gebuchtes VerwaltungsObjekt
	 * @param time Uhrzeit
	 * @param date Datum
	 */
	public void createNewBuchung(VerwaltungsObjekt e1, VerwaltungsObjekt e2, String time, String date) {
		addBuchung(new Buchung(e1, e2, time, date, getNextID()));
	}
	/**
	 * Methode zum hinzufuegen einer neuen Buchung.
	 * @param b Buchungs Objekt
	 */
	public void addBuchung(Buchung b) {
		if (isBuchungPossible(b)) {
			System.out.println("Buchung leider nicht moeglich!");
		} else {
			getBuchungen().add(b);
			XMLHandler.updateXML(this);
			System.out.println("Buchung wurde gespeichert.");
		}
	}
	
	/**
	 * Methode zum loeschen einer Buchung<br>
	 * Prueft ob die Buchung vorhanden ist, loescht sie aus der ArrayList und updated die XML.
	 * @param bDel Buchungs Objekt
	 * @return true = Buchung geloescht, false = Buchung konnte nicht geloescht werden.
	 */
	public boolean deleteBuchung(Buchung bDel){
		for (Buchung b : getBuchungen()){
			if (b.equals(bDel)){
				getBuchungen().remove(bDel);
				XMLHandler.updateXML(this);
				System.out.println("Buchung wurde geloescht!");
				return true;
			}
		}
		System.out.println("Die ausgewaehlte Buchung konnte leider nicht geloescht werden!,");
		return false;
	}
	/**
	 * Methode die ein Buchungs Objekt zurueck gibt anhand der ID.
	 * @param ID ID der gewuenschten Buchung
	 * @return Buchungs Objekt
	 */
	public Buchung getBuchung(int ID) {
		for (Buchung b : getBuchungen()) {
			if (b.getID() == ID) {
				return b;
			}
		}
		return null;
	}
	/**
	 * Methode die eine Liste der aktuellen Buchungen zurueck gibt.
	 * @return ArrayList der Buchungen
	 */
	public ArrayList<Buchung> getBuchungen() {
		return buchungen;
	}
	/**
	 *  Methode zur generierung der ID. Liest die aktuell hoechste verwendete ID der Buchungen
	 * @return neue ID als Integer
	 */
	public int getNextID() {
		int highID = 0;
		for (Buchung b : getBuchungen()) {
			if (b.getID() > highID)
				highID = b.getID();
		}
		highID++;
		return highID;
	}
	/**
	 * Methode zur Ueberpruefung ob eine Buchung moeglich ist.<br>
	 * Checkt auf doppelte Buchungen, ueberschneidende Zeitraeume und Ueberschneidungen der Parteien.
	 * @param bNew zu pruefende Buchung
	 * @return true = Buchung moeglich, flase = Buchung nicht moeglich
	 */
	public boolean isBuchungPossible(Buchung bNew) {
		for (Buchung b : getBuchungen()) {
			if (b.ID == 0){
				continue;
			}
			if (!b.equals(bNew)) {
				if (b.getBuchungsElemente().getElement0().equals(bNew.getBuchungsElemente().getElement0())
						|| b.getBuchungsElemente().getElement0().equals(bNew.getBuchungsElemente().getElement1())
						|| b.getBuchungsElemente().getElement1().equals(bNew.getBuchungsElemente().getElement0())
						|| b.getBuchungsElemente().getElement1().equals(bNew.getBuchungsElemente().getElement1())) {
					if (b.getBuchungsDatum().equals(bNew.getBuchungsDatum())
							&& b.getBuchungsZeit().equals(bNew.getBuchungsZeit())) {
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		}
		return false;

	}
}
