package verwaltungs_Objekte.buchungen;

import erp_software.Pair;
import interfaces.VerwaltungsObjekt;
import verwaltungs_Objekte.human_resources.Mitarbeiter;

/**
 * Klasse zur Speicherung einer Buchung zwischen einem {@link verwaltungs_Objekte.human_resources.Mitarbeiter Mitarbeiter}
 * und einem anderen {@link interfaces.VerwaltungsObjekt VerwaltungsObjekt}.
 * @see interfaces.VerwaltungsObjekt
 * @see verwaltungs_Objekte.human_resources.Mitarbeiter
 */
public class Buchung extends VerwaltungsObjekt {

	private Pair<VerwaltungsObjekt, VerwaltungsObjekt> buchungsElemente;
	private String buchungsZeit, buchungsDatum;
	/**
	 * Konstruktor zum Anlegen einer neuen Buchung.
	 * @param e1 das buchende VerwaltungsObjekt (Mitarbeiter)
	 * @param e2 das gebuchte VerwaltungsObjekt
	 * @param zeit Uhrzeit
	 * @param datum Datum
	 * @param id eindeutige Identifikationsnummer
	 */
	public Buchung(VerwaltungsObjekt e1, VerwaltungsObjekt e2, String zeit, String datum, int id) {
		super(id);
		this.buchungsElemente = new Pair<VerwaltungsObjekt, VerwaltungsObjekt>(e1, e2);
		this.buchungsDatum = datum;
		this.buchungsZeit = zeit;
	}

	public Pair<VerwaltungsObjekt, VerwaltungsObjekt> getBuchungsElemente() {
		return buchungsElemente;
	}

	public void setBuchungsElemente(Pair<VerwaltungsObjekt, VerwaltungsObjekt> buchungsElemente) {
		this.buchungsElemente = buchungsElemente;
	}

	public String getBuchungsZeit() {
		return buchungsZeit;
	}

	public void setBuchungsZeit(String buchungsZeit) {
		this.buchungsZeit = buchungsZeit;
	}

	public String getBuchungsDatum() {
		return buchungsDatum;
	}

	public void setBuchungsDatum(String buchungsDatum) {
		this.buchungsDatum = buchungsDatum;
	}

	public int getID() {
		return ID;
	}
	/**
	 * "Null"-Konstruktor zur initialisierung im XML-Handler.
	 * @see erp_software.XMLHandler#initXMLs(Object)
	 */
	public Buchung() {
		super(0);
		this.buchungsElemente = new Pair<VerwaltungsObjekt, VerwaltungsObjekt>(new Mitarbeiter(), new Mitarbeiter());
		this.buchungsDatum = null;
		this.buchungsZeit = null;
	}

	public boolean equals(Buchung b2) {
		if (ID == 0 && b2.ID == 0){
			return true;
		}else if(ID == 0 || b2.ID == 0){
			return false;
		}
		return (buchungsElemente.getElement0().equals(b2.buchungsElemente.getElement0())
				&& buchungsElemente.getElement1().equals(b2.buchungsElemente.getElement1())
				&& buchungsZeit.equals(b2.buchungsZeit) && buchungsDatum.equals(b2.buchungsDatum));
	}
	/**
	 * Methode zur Ausgabe aller Buchungsinformationen auf der Konsole.
	 */
	public String toString() {
			return ("Buchung von: \n" + buchungsElemente.getElement0().toString() + "\n\nund \n\n"
					+ buchungsElemente.getElement1().toString() + "\n\nDatum: " + buchungsDatum + "\nZeit: "
					+ buchungsZeit);
	}
}
