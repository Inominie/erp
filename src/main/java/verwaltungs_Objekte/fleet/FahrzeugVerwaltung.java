package verwaltungs_Objekte.fleet;

import java.util.ArrayList;
import java.util.List;

import erp_software.XMLHandler;
import interfaces.Verwaltung;

/**
 * Klasse zur Verwaltung der Fahrzeuge.<br>
 * Aus dieser Klasse wird die XML-Datenbank der Fahrzeuge erstellt im {@link erp_software.XMLHandler XMLHandler}.<br>
 * Ausserdem wird hier die Fahrzeugnummer(ID) erstellt {@link #getNextID() getNextID}.
 *
 *@see Verwaltung
 *@see Fahrzeug
 */
public class FahrzeugVerwaltung implements Verwaltung {
	List<Fahrzeug> autos = new ArrayList<Fahrzeug>();
	/**
	 * Standard Konstruktor
	 */

	public FahrzeugVerwaltung() {
	}
	/**
	 * Methode zum anlegen eines neuen Fahrzeuges anhand von Input Daten.<br>
	 * Ruft die {@link #fahrzeugHinzufuegen(Fahrzeug) fahrzeugHinzufuegen} Methode auf.
	 * @param name Name
	 */
	public void createFahrzeug(String name){
		fahrzeugHinzufuegen(new Fahrzeug(name, getNextID()));
	}
	/**
	 * Methode zum Hinzufuegen eines neuen Fahrzeuges.<br>
	 * Updatet die XML-Datenbank nach dem Hinzufuegen des neuen Fahrzeuges.
	 * @param auto Fahrzeug Objekt
	 * @see XMLHandler#updateXML(Object)
	 */
	public void fahrzeugHinzufuegen(Fahrzeug auto) {
		for (Fahrzeug auto2 : autos) {
			if (auto.equals(auto2)) {
				System.out.println("Das Fahrzeug: " + auto.toString() + "ist bereits hinzugefuegt worden.");
				break;
			}
		}
		autos.add(auto);
		XMLHandler.updateXML(this);
		System.out.println("Das Fahrzeug: " + auto.toString() + "\nwurde hinzugefuegt.");
	}
	/**
	 * Methode zum Loeschen eines Fahrzeuges.<br>
	 * Prueft ob das Fahrzeug vorhanden ist, loescht es aus der ArrayList und updated die XML.
	 * @param fahrzeugnummer Fahrzeugnummer
	 * @see XMLHandler#updateXML(Object)
	 */
	public void fahrzeugLoeschen(int fahrzeugnummer) {
		for (Fahrzeug auto2 : autos) {
			if (auto2.getFahrzeugnummer() == fahrzeugnummer) {
				autos.remove(auto2);
				XMLHandler.updateXML(this);
				System.out.println("Das ausgewaehlte Fahrzeug " + auto2.toString() + " wurde erfolgreich geloescht.");
				return;
			}
		}
		System.out.println("Das ausgewählte Farhzeug konnte leider nicht geloescht werden.");
	}

	/**
	 * Methode zum Berechnen des aktuellen Verbrauchs.<br>
	 * Berechnet aus dem Kilometerstand und der Literangabe an akutellen Verbrauch.
	 * @param kilometerstand Kilometerstand
	 * @param liter Literangabe
	 * @param durchschnittsverbrauch Durchschnittsverbrauch
	 * @see XMLHandler#updateXML(Object)
	 */
	public void aktuellerVerbrauch(double kilometerstand, double liter, double durchschnittsverbrauch) {
		durchschnittsverbrauch = liter * 100 / kilometerstand;
		System.out.println(
				"Der aktuelle Durchschnittsverbrauch beträgt " + durchschnittsverbrauch + "Liter auf 100 Kilometern.");
	}
	/**
	 * Methode zur generierung der ID. Liest die aktuell hoechste verwendete Fahrzeugnummer
	 * aus und gibt die naechst hoehere zurueck.
	 * @return neue ID als Integer
	 * @see Verwaltung
	 */
	public int getNextID(){
		int highID = 0;
		for (Fahrzeug auto : autos) {
			if (auto.getFahrzeugnummer() > highID)
				highID = auto.getFahrzeugnummer();
		}
		highID++;
		return highID;
	}
	
	/**
	 * Methode die ein Fahrzeug Objekt anhand einer Fahrzeugnumme zurueck gibt .
	 * @param id ID des gewuenschten Fahrzeuges
	 * @return Fahrzeug Objekt
	 */
	public Fahrzeug getFahrzeug(int id){
		for (Fahrzeug f : getFahrzeuge()){
			if (f.ID == id){
				return f;
			}
		}
		return null;
	}
	/**
	 * Methode die die aktuelle Fahrzeugliste zurueck gibt.
	 * @return Fahrzeugliste
	 */
	public List<Fahrzeug> getFahrzeuge(){
		return autos;
	}
}