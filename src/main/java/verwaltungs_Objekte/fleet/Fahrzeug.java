package verwaltungs_Objekte.fleet;

import interfaces.VerwaltungsObjekt;
/**
 * Klasse die zur Speicherung aller Fahrzeug bezogenen Datein dient.
 *@see FahrzeugVerwaltung
 */
public class Fahrzeug extends VerwaltungsObjekt{

	private String name;
	private boolean buchungMoeglich;
	private double kilometerstand;
	private double liter;
	private double durchschnittsverbrauch;
	
	/**
	 * Konstruktor
	 * @param name Fahrzeug Name
	 * @param fahrzeugnummer eindeutige Identifikationsnummer
	 */
	public Fahrzeug(String name, int fahrzeugnummer) {
		super(fahrzeugnummer);
		this.name = name;
		boolean buchungMoeglich = true;
	}
	/**
	 * "Null"-Konstruktor zur initialisierung im XML-Handler.
	 * @see erp_software.XMLHandler#initXMLs(Object)
	 */
	public Fahrzeug(){
		super(0);
		name = "";
		buchungMoeglich = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFahrzeugnummer() {
		return ID;
	}

	public boolean isBuchungMoelgich() {
		return buchungMoeglich;
	}

	public double getKilometerstand() {
		return kilometerstand;
	}

	public double getLiter() {
		return liter;
	}

	public double getDurchschnittsverbrauch() {
		return durchschnittsverbrauch;
	}

	public boolean equals(Fahrzeug auto1) {
		if (auto1.ID == 0){
			return false;
		}
		return (this.name == auto1.getName()) && (this.ID == auto1.getFahrzeugnummer());
	}

	@Override
	/**
	 * Methode zur Ausgabe der Fahrzeug Daten auf der Konsole
	 */
	public String toString() {
		return "Das Auto mit dem Namen: " + name + "\n hat die Fahrzeugnummer: " + ID
				+ "\n Der aktuelle Kilometerstand beträgt: " + kilometerstand;
	}
}
