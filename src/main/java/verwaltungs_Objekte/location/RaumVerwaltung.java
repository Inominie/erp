package verwaltungs_Objekte.location;

import java.util.ArrayList;
import java.util.List;

import erp_software.XMLHandler;
import interfaces.Verwaltung;
/**
 * Klasse zur Verwaltung der Raeume.<br>
 * Aus dieser Klasse wird die XML-Datenbank der Raeume erstellt im {@link erp_software.XMLHandler XMLHandler}.<br>
 * Au�erdem wird hier die ID der Raeume erstellt {@link #getNextID() getNextID}.
 *
 *@see Verwaltung
 *@see Raum
 */
public class RaumVerwaltung implements Verwaltung {
	List<Raum> raeume = new ArrayList<Raum>();
	/**
	 * Standard Konstruktor
	 */
	public RaumVerwaltung() {

	}
	/**
	 * Methode zum anlegen eines neuen Raums.<br>
	 * Ruft die {@link #addRaum(Raum) addRaum} Methode auf.
	 * @param name Raumname
	 * @param grundflaeche Grundflaeche
	 * @param raumhoehe Hoehe
	 */
	public void createRaum(String name, double grundflaeche, double raumhoehe) {
		addRaum(new Raum(name, getNextID(), grundflaeche, raumhoehe));
	}
	/**
	 * Methode zum hinzufuegen eines neuen Raums.<br>
	 * Updatet die XML-Datenbank nach dem Hinzufuegen des neuen Raums.
	 * @param r1 Raum Objekt
	 * @see XMLHandler#updateXML(Object)
	 */
	public void addRaum(Raum r1) {	
		for (Raum r2 : raeume) {
			if (r1.equals(r2)) {
				System.out.println("Raum" + r1.toString() + "bereits angelegt.");
				break;
			}
		}
		raeume.add(r1);
		XMLHandler.updateXML(this);
		System.out.println("Raum" + r1.toString() + "wurde erfolgreich angelegt.");
	}
	/**
	 * Methode zum loeschen eines Raums.<br>
	 * Prueft ob der Raum vorhanden ist, loescht Ihn und updated die XML.
	 * @param raumNr eindeutige Identifikationsnummer
	 * @see XMLHandler#updateXML(Object)
	 */
	public void raumLoeschen(int raumNr) {
		for (Raum r2 : raeume) {
			if (r2.ID == raumNr) {
				raeume.remove(r2);
				XMLHandler.updateXML(this);
				System.out.println("Ihr Raum wurde erfolgreich gelöscht!");
				return;
			}
		}
		System.out.println("Raum konnte nicht geloescht werden.");
	}
	/**
	 * Berechnet den Rauminhalt anhand von Grundflaeche und Hoehe
	 * @param grundflaeche Grundflaeche
	 * @param raumhoehe Hoehe
	 */
	public void raumInhalt(float grundflaeche, float raumhoehe) {
		float rauminhalt;
		rauminhalt = grundflaeche * raumhoehe;
		System.out.println("Der Rauminhalt betraegt: " + rauminhalt);
	}
	/**
	 * Methode zur generierung der ID. Liest die aktuell hoechste verwendete ID der Raeume
	 * aus und gibt die naechst hoehere zurueck.
	 * @return neue ID als Integer
	 * @see Verwaltung
	 */
	public int getNextID() {
		int highID = 0;
		for (Raum m : raeume) {
			if (m.getRaumNr() > highID)
				highID = m.getRaumNr();
		}
		highID++;
		return highID;
	}
	/**
	 * Gibt ein Raum Objekt anhand der ID zurueck.
	 * @param pID ID
	 * @return Raum Objekt
	 */
	public Raum getRaum(int pID){
		for(Raum r: raeume){
			if(r.getRaumNr() == pID){
				return r;
			}
		}
		return null;
	}
	/**
	 * Gibt die aktuelle ArrayList mit allen Raeumen zurueck.
	 * @return ArraysList der Raeume
	 */
	public List<Raum> getRaeume(){
		return raeume;
	}

}
