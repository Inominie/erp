package verwaltungs_Objekte.location;

import interfaces.VerwaltungsObjekt;
/**
 * Klasse zur Speicherung der Informationen eines Raums.
 *@see RaumVerwaltung
 */
public class Raum extends VerwaltungsObjekt {

	private String name;
	private double grundflaeche;
	private double raumhoehe;
	private boolean buchungMoeglich;
	
	/**
	 * Konstruktor zum Anlegen eines neuen Raumes.
	 * @param name Raumname
	 * @param raumnr eindeutige Identifikationsnummer
	 * @param grundflaeche Grundflaeche
	 * @param raumhoehe Hoehe
	 */
	public Raum(String name, int raumnr, double grundflaeche, double raumhoehe) {
		super(raumnr);
		this.name = name;
		this.grundflaeche = grundflaeche;
		this.raumhoehe = raumhoehe;
		this.buchungMoeglich = true;
	}
	/**
	 * "Null"-Konstruktor zur initialisierung im XML-Handler.
	 * @see erp_software.XMLHandler#initXMLs(Object)
	 */
	public Raum(){
		super(0);
		this.name = null;
		this.grundflaeche = 0;
		this.raumhoehe = 0;
		this.buchungMoeglich = false;
	}

	public String getName() {
		return name;
	}

	public int getRaumNr() {
		return ID;
	}

	public double getGrundflaeche() {
		return grundflaeche;
	}

	public double getRaumhoehe() {
		return raumhoehe;
	}

	public boolean isBuchungMoeglich() {
		return buchungMoeglich;
	}

	public void setBuchungMoeglich(boolean buchungMoeglich) {
		this.buchungMoeglich = buchungMoeglich;
	}

	public boolean equals(Raum r1) {
		if (r1.ID == 0){
			return false;
		}
		return (this.name == r1.getName() && this.ID == r1.getRaumNr());
	}
	/**
	 * Methode zur Ausgabe aller relevanten Informationen eines Raumes auf der Konsole.
	 */
	public String toString() {
		return "Name:" + name + "\n Raumnummer:" + ID + "\n Grundflaeche:" + grundflaeche + "\n Raumhoehe:"
				+ raumhoehe + "\n";
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGrundflaeche(double grundflaeche) {
		this.grundflaeche = grundflaeche;
	}

	public void setRaumhoehe(double raumhoehe) {
		this.raumhoehe = raumhoehe;
	}
}
