package command_handler;

import erp_software.Erp;
import erp_software.XMLHandler;
import interfaces.Command;
import verwaltungs_Objekte.location.Raum;
import verwaltungs_Objekte.location.RaumVerwaltung;
/**
 * Klasse zur Verwaltung von Raumkommandos <br>
 * Aus dieser Klasse werden die einzelnen Funktionalit�ten der 
 * Klasse {@link verwaltungs_Objekte.location.RaumVerwaltung} aufgerufen. <br>
 *
 *@see Command
 */
public class RaumCommands implements Command {

	private String command, singleCommand;

	/**
	 * Commandhandler zum Aufrufen der einzelnen Funktionalitaeten
	 * Der Commandhandler wertet die Eingabe hinsichtlich des Kommandos aus und ruft dann die jeweilige Methode auf.
	 */
	public void startScan() {
		displayConfig("Raum");
		command = "";
		singleCommand = "";
		while (!command.equals("BACK")) {
			if (Erp.sc.hasNext()) {
				command = Erp.sc.nextLine();
				singleCommand = command.split("\\s+")[0].toUpperCase();
				command = command.toUpperCase();
				switch (singleCommand) {
				case "HELP": {
					displayConfig("Raum");
					break;
				}
				case "NEW": {
					callCommandNEW();
					break;
				}
				case "LSA": {
					callCommandLSA();
					break;
				}
				case "LS": {
					callCommandLS(command);
					break;
				}
				case "UP": {
					callCommandUP(command);
					break;
				}
				case "DEL": {
					callCommandDEL(command);
					break;
				}
				case "INIT": {
					XMLHandler.initXMLs(new RaumVerwaltung());
					break;
				}
				case "BACK": {
					Erp.startScan();
					break;
				}

				}
			}
		}
	}

	private void callCommandUP(String command) {
		int id = getCommandID(command);
		if (id != 0) {
			RaumVerwaltung rv = (RaumVerwaltung) XMLHandler.getXML(new RaumVerwaltung());
			Raum r = rv.getRaum(id);
			if (r != null) {
				System.out.println("Grundflaeche eingeben:");
				r.setGrundflaeche(Double.parseDouble(Erp.sc.next()));
				System.out.println("Raumhoehe eingeben:");
				r.setRaumhoehe(Double.parseDouble(Erp.sc.next()));
				System.out.println("Namen eingeben:");
				r.setName(Erp.sc.next());
				XMLHandler.updateXML(rv);
			} else {
				System.out.println("Raum mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandDEL(String command) {
		int id = getCommandID(command);

			RaumVerwaltung rv = (RaumVerwaltung) XMLHandler.getXML(new RaumVerwaltung());
			rv.raumLoeschen(id);
	}

	private void callCommandLS(String command) {
		int id = getCommandID(command);

		if (id != 0) {
			RaumVerwaltung ma = (RaumVerwaltung) XMLHandler.getXML(new RaumVerwaltung());
			Raum m = ma.getRaum(id);
			if (m != null) {
				System.out.println(m.toString());
			} else {
				System.out.println("Raum mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandLSA() {
		RaumVerwaltung ma = (RaumVerwaltung) XMLHandler.getXML(new RaumVerwaltung());
		ma.getRaeume().forEach((m) -> System.out.println(m.toString() + "\n"));

	}

	private void callCommandNEW() {
		String name = null, hoehe = null, flaeche = null;

		while (name == null || hoehe == null || flaeche == null) {

			if (name == null) {
				System.out.println("Bitte Namen eingeben:");
				name = Erp.sc.next();
			}
			if (hoehe == null) {
				System.out.println("Bitte Raumhoehe eingeben:");
				hoehe = Erp.sc.next();
			}
			if (flaeche == null) {
				System.out.println("Bitte Raumflaeche eingeben:");
				flaeche = Erp.sc.next();
			}
		}
		try {
			RaumVerwaltung rv = (RaumVerwaltung) XMLHandler.getXML(new RaumVerwaltung());
			rv.createRaum(name, Double.parseDouble(flaeche), Double.parseDouble(hoehe));
		} catch (RuntimeException ex) {
			System.out.println(ex.getMessage());
		}
	}
}
