package command_handler;

import erp_software.Erp;
import erp_software.Pair;
import erp_software.XMLHandler;
import interfaces.Command;
import interfaces.VerwaltungsObjekt;
import verwaltungs_Objekte.buchungen.Buchung;
import verwaltungs_Objekte.buchungen.BuchungsVerwaltung;
import verwaltungs_Objekte.fleet.Fahrzeug;
import verwaltungs_Objekte.fleet.FahrzeugVerwaltung;
import verwaltungs_Objekte.human_resources.Mitarbeiter;
import verwaltungs_Objekte.human_resources.MitarbeiterVerwaltung;
import verwaltungs_Objekte.it.Device;
import verwaltungs_Objekte.it.DeviceVerwaltung;
import verwaltungs_Objekte.location.Raum;
import verwaltungs_Objekte.location.RaumVerwaltung;

/**
 * Klasse zur Verwaltung von Buchungskommandos <br>
 * Aus dieser Klasse werden die einzelnen Funktionalit�ten der 
 * Klasse {@link verwaltungs_Objekte.buchungen.BuchungsVerwaltung} aufgerufen. <br>
 *
 *@see Command
 */
public class BuchungCommands implements Command {

	private String command, singleCommand;

	/**
	 * Commandhandler zum Aufrufen der einzelnen Funktionalitaeten
	 * Der Commandhandler wertet die Eingabe hinsichtlich des Kommandos aus und ruft dann die jeweilige Methode auf.
	 */
	@Override
	public void startScan() {
		displayConfig("Buchung");
		command = "";
		singleCommand = "";

		while (!command.equals("BACK")) {
			if (Erp.sc.hasNext()) {
				command = Erp.sc.nextLine();
				singleCommand = command.split("\\s+")[0].toUpperCase();
				command = command.toUpperCase();
				switch (singleCommand) {
				case "HELP": {
					displayConfig("Buchung");
					break;
				}
				case "NEW": {
					callComandBNEW();
					break;
				}
				case "LSA": {
					callCommandLSAB();
					break;
				}
				case "LS": {
					callCommandLSB(command);
					break;
				}
				case "DEL": {
					callCommandDELB(command);
					break;
				}
				case "INIT": {
					XMLHandler.initXMLs(new BuchungsVerwaltung());
					break;
				}
				case "BACK": {
					Erp.startScan();
					break;
				}
				}
			}
		}
	}

	/**
	 * Commandhandler zum L�schen einer Buchung<br>
	 * Der Commmandhandler wertet die Eingabe aus hinsichtlich der ID aus, liest die XML-Datei ein und 
	 * ruft die Methode {@link verwaltungs_Objekte.buchungen.BuchungsVerwaltung#deleteBuchung(Buchung)} zum L�schen der Buchung auf. 
	 * @param command2 Eingabe
	 * @see XMLHandler#getXML(Object)
	 */
	private void callCommandDELB(String command2) {
		int id = getCommandID(command2);

			BuchungsVerwaltung bv = (BuchungsVerwaltung) XMLHandler.getXML(new BuchungsVerwaltung());
			Buchung b = bv.getBuchung(id);
			if (!(b == null)) {
				bv.deleteBuchung(b);
			}
	}
	/**
	 * Commandhandler zum Anzeigen einer Buchung<br>
	 * Der Commmandhandler wertet die Eingabe aus hinsichtlich der ID aus, liest die XML-Datei ein und 
	 * ruft die Methode {@link verwaltungs_Objekte.buchungen.BuchungsVerwaltung#getBuchung(int)} zum Anzeigen der Buchung auf. 
	 * @param command2
	 * @see XMLHandler#getXML(Object)
	 */
	private void callCommandLSB(String command2) {
		int id = getCommandID(command2);

		if (id != 0) {
			BuchungsVerwaltung bv = (BuchungsVerwaltung) XMLHandler.getXML(new BuchungsVerwaltung());
			System.out.println(bv.getBuchung(id).toString());
		} else {
			System.out.println("Buchung mit ID " + id + " nicht vorhanden!");
		}
	}

    /**
     * Commandhandler zum Anzeigen aller Buchungen<br>
     * Der Commandhandler liest die XML-Datei ein und ruft die Methode  {@link verwaltungs_Objekte.buchungen.BuchungsVerwaltung#getBuchungen()} zum Anzeigen aller Buchungen auf.
     * @see XMLHandler#getXML(Object)
     */
	private void callCommandLSAB() {
		BuchungsVerwaltung bv = (BuchungsVerwaltung) XMLHandler.getXML(new BuchungsVerwaltung());

		for (Buchung b : bv.getBuchungen()) {
			System.out.println(b.toString() + "\n");
		}

	}

	/**
	 * Commandhandler zum Anlegen einer Buchung<br>
	 * Der Commandhandler fragt ab welcher Mitarbeiter was gebuchen m�chte, liest die jeweilige XML-Datei ein und
	 * anschlie�end wird Datum und Uhrzeit eingegben.
	 * @see Mitarbeiter
	 * @see Fahrzeug
	 * @see Raeume
	 * @see Devices
	 * @see XMLHandler#getXML(Object)
	 */
	private void callComandBNEW() {
		String datum = null, zeit = null;
		Pair<VerwaltungsObjekt, VerwaltungsObjekt> elements = null;
		int id, kat, id2;
		System.out.println("Bitte Mitarbeiter ID eingeben:");
		id = Erp.sc.nextInt();
		Erp.sc.nextLine();

		MitarbeiterVerwaltung ma = (MitarbeiterVerwaltung) XMLHandler.getXML(new MitarbeiterVerwaltung());

		Mitarbeiter m = ma.getMitarbeiter(id);
		if (!(m == null)) {
			System.out.println("Was soll gebucht werden?\n1 = Device\n2 = Fahrzeug\n" + "3 = Meeting\n4 = Raum");
			kat = Erp.sc.nextInt();
			Erp.sc.nextLine();

			while (elements == null) {
				switch (kat) {
				case 1: {
					System.out.println("Ger�te ID eingeben:");
					id2 = Erp.sc.nextInt();
					Erp.sc.nextLine();
					DeviceVerwaltung dv = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());
					Device d = dv.getDevice(id2);
					if (!(d == null)) {
						elements = new Pair<VerwaltungsObjekt, VerwaltungsObjekt>(m, d);
					} else {
						System.out.println("ID nicht vorhanden!");
					}
					break;
				}
				case 2: {
					System.out.println("Fahrzeug ID eingeben:");
					id2 = Erp.sc.nextInt();
					Erp.sc.nextLine();
					FahrzeugVerwaltung fv = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());
					Fahrzeug f = fv.getFahrzeug(id2);
					if (!(f == null)) {
						elements = new Pair<VerwaltungsObjekt, VerwaltungsObjekt>(m, f);
					}
					break;
				}
				case 3: {
					System.out.println("Mitarbeiter ID eingeben:");
					id2 = Erp.sc.nextInt();
					Erp.sc.nextLine();
					Mitarbeiter m2 = ma.getMitarbeiter(id2);
					if (!(m2 == null)) {
						elements = new Pair<VerwaltungsObjekt, VerwaltungsObjekt>(m, m2);
					}
					break;
				}
				case 4: {
					System.out.println("Raum ID eingeben:");
					id2 = Erp.sc.nextInt();
					Erp.sc.nextLine();
					RaumVerwaltung rv = (RaumVerwaltung) XMLHandler.getXML(new RaumVerwaltung());
					Raum r = rv.getRaum(id2);
					if (!(r == null)) {
						elements = new Pair<VerwaltungsObjekt, VerwaltungsObjekt>(m, r);
					}
					break;
				}
				default: {
					System.out.println("Falsche Eingabe!");
				}
				}
			}
			
			while (datum == null || zeit == null) {
				System.out.println("Buchungsdatum eingeben:");
				datum = Erp.sc.nextLine();

				if (datum != null) {
					System.out.println("Uhrzeit eingeben:");
					zeit = Erp.sc.nextLine();
				}
			}

			BuchungsVerwaltung bv = (BuchungsVerwaltung) XMLHandler.getXML(new BuchungsVerwaltung());
			bv.createNewBuchung(elements.getElement0(), elements.getElement1(), zeit, datum);

		} else {
			System.out.println("Mitarbeiter nicht vorhanden!");
		}

	}

}
