package command_handler;

import erp_software.Erp;
import erp_software.XMLHandler;
import interfaces.Command;
import verwaltungs_Objekte.human_resources.Mitarbeiter;
import verwaltungs_Objekte.human_resources.MitarbeiterVerwaltung;
/**
 * Klasse zur Verwaltung von Mitarbeiterkommandos <br>
 * Aus dieser Klasse werden die einzelnen Funktionalit�ten der 
 * Klasse {@link verwaltungs_Objekte.human_resources.MitarbeiterVerwaltung} aufgerufen. <br>
 *
 *@see Command
 */
public class MitarbeiterCommands implements Command {

	private String command, singleCommand;

	/**
	 * Commandhandler zum Aufrufen der einzelnen Funktionalitaeten
	 * Der Commandhandler wertet die Eingabe hinsichtlich des Kommandos aus und ruft dann die jeweilige Methode auf.
	 */
	public void startScan() {
		displayConfig("Mitarbeiter");
		command = "";
		singleCommand = "";
		while (!command.equals("BACK")) {
			if (Erp.sc.hasNext()) {
				command = Erp.sc.nextLine();
				singleCommand = command.split("\\s+")[0].toUpperCase();
				command = command.toUpperCase();
				switch (singleCommand) {
				case "HELP": {
					displayConfig("Mitarbeiter");
					break;
				}
				case "NEW": {
					callCommandMNEW();
					break;
				}
				case "LSA": {
					callCommandLSAM();
					break;
				}
				case "LS": {
					callCommandLSM(command);
					break;
				}
				case "UP": {
					callCommandCM(command);
					break;
				}
				case "DEL": {
					callCommandDELM(command);
					break;
				}
				case "INIT": {
					XMLHandler.initXMLs(new MitarbeiterVerwaltung());
					break;
				}
				case "BACK": {
					Erp.startScan();
					break;
				}

				}
			}
		}
	}

	private void callCommandCM(String command) {
		int id = getCommandID(command);
		if (id != 0) {
			MitarbeiterVerwaltung ma = (MitarbeiterVerwaltung) XMLHandler.getXML(new MitarbeiterVerwaltung());
			Mitarbeiter m = ma.getMitarbeiter(id);
			if (m != null) {
				System.out.println("EMail eingeben:");
				m.setEmail(Erp.sc.next());
				System.out.println("Telefonnummer eingeben;");
				m.setTelefon(Long.parseLong(Erp.sc.next()));
				XMLHandler.updateXML(ma);
			} else {
				System.out.println("Mitarbeiter mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandDELM(String command) {
		int id = getCommandID(command);

			MitarbeiterVerwaltung ma = (MitarbeiterVerwaltung) XMLHandler.getXML(new MitarbeiterVerwaltung());
			Mitarbeiter m = ma.getMitarbeiter(id);
			if (m != null) {
				if (!ma.deleteMitarbeiter(m)) {
					System.out.println("Fehler");
				}
			} else {
				System.out.println("Mitarbeiter mit ID: " + id + " nicht vorhanden!");
			}
	}

	private void callCommandLSM(String command) {
		int id = getCommandID(command);

		if (id != 0) {
			MitarbeiterVerwaltung ma = (MitarbeiterVerwaltung) XMLHandler.getXML(new MitarbeiterVerwaltung());
			Mitarbeiter m = ma.getMitarbeiter(id);
			if (m != null) {
				System.out.println(m.toString());
			} else {
				System.out.println("Mitarbeiter mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandLSAM() {
		MitarbeiterVerwaltung ma = (MitarbeiterVerwaltung) XMLHandler.getXML(new MitarbeiterVerwaltung());
		ma.getMitarbeiter().forEach((m) -> System.out.println(m.toString() + "\n"));

	}

	private void callCommandMNEW() {
		String vName = null, nName = null, bDay = null;

		while (vName == null || nName == null || bDay == null) {

			if (vName == null) {
				System.out.println("Bitte Vorname eingeben:");
				vName = Erp.sc.next();
			}
			if (nName == null) {
				System.out.println("Bitte Nachname eingeben:");
				nName = Erp.sc.next();
			}
			if (bDay == null) {
				System.out.println("Bitte Geburtsdatum eingeben:");
				bDay = Erp.sc.next();
			}
		}
		try {
			MitarbeiterVerwaltung ma = (MitarbeiterVerwaltung) XMLHandler.getXML(new MitarbeiterVerwaltung());
			ma.createNewMitarbeiter(vName, nName, bDay);
		} catch (RuntimeException ex) {
			System.out.println("Exception " + ex.getMessage());
		}
	}

}
