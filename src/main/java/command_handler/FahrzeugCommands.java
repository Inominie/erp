package command_handler;

import erp_software.Erp;
import erp_software.XMLHandler;
import interfaces.Command;
import verwaltungs_Objekte.fleet.Fahrzeug;
import verwaltungs_Objekte.fleet.FahrzeugVerwaltung;

/**
 * Klasse zur Verwaltung von Fahrzeugkommandos <br>
 * Aus dieser Klasse werden die einzelnen Funktionalit�ten der 
 * Klasse {@link verwaltungs_Objekte.fleet.FahrzeugVerwaltung} aufgerufen. <br>
 *
 *@see Command
 */
public class FahrzeugCommands implements Command {

	private String command, singleCommand;

	/**
	 * Commandhandler zum Aufrufen der einzelnen Funktionalitaeten
	 * Der Commandhandler wertet die Eingabe hinsichtlich des Kommandos aus und ruft dann die jeweilige Methode auf.
	 */
	public void startScan() {
		displayConfig("Fahrzeug");
		command = "";
		singleCommand = "";
		while (!command.equals("BACK")) {
			if (Erp.sc.hasNext()) {
				command = Erp.sc.nextLine();
				singleCommand = command.split("\\s+")[0].toUpperCase();
				command = command.toUpperCase();
				switch (singleCommand) {
				case "HELP": {
					displayConfig("Fahrzeug");
					break;
				}
				case "NEW": {
					callCommandNEW();
					break;
				}
				case "LSA": {
					callCommandLSA();
					break;
				}
				case "LS": {
					callCommandLS(command);
					break;
				}
				case "LSTEC": {
					callCommandLSTEC(command);
					break;
				}
				case "UP": {
					callCommandUP(command);
					break;
				}
				case "DEL": {
					callCommandDEL(command);
					break;
				}
				case "INIT": {
					XMLHandler.initXMLs(new FahrzeugVerwaltung());
					break;
				}
				case "BACK": {
					Erp.startScan();
					break;
				}

				}
			}
		}
	}

	private void callCommandLSTEC(String command2) {
		int id = getCommandID(command);

		if (id != 0) {
			FahrzeugVerwaltung ma = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());
			Fahrzeug m = ma.getFahrzeug(id);
			if (m != null) {
				System.out.println(m.toString());
			} else {
				System.out.println("Fahrzeug mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandUP(String command) {
		int id = getCommandID(command);
		if (id != 0) {
			FahrzeugVerwaltung dk = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());
			Fahrzeug d = dk.getFahrzeug(id);
			if (d != null) {
				System.out.println("Name eingeben:");
				d.setName(Erp.sc.next());
				XMLHandler.updateXML(dk);
			} else {
				System.out.println("Fahrzeug mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandNEW() {
		String name = null;

		while (name == null) {
			if (name == null) {
				System.out.println("Bitte Geraetenamen eingeben: ");
				name = Erp.sc.next();
		}
		try {
			FahrzeugVerwaltung dk = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());
			dk.createFahrzeug(name);
		} catch (RuntimeException ex) {
			System.out.println(ex.getMessage());
		}
	}
	}

	private void callCommandLS(String command) {
		int id = getCommandID(command);

		if (id != 0) {
			FahrzeugVerwaltung ma = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());
			Fahrzeug m = ma.getFahrzeug(id);
			if (m != null) {
				System.out.println(m.toString());
			} else {
				System.out.println("Fahrzeug mit ID: " + id + " nicht vorhanden!");
			}
		}
	}

	private void callCommandLSA() {
		FahrzeugVerwaltung da = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());

		for (Fahrzeug d : da.getFahrzeuge()) {
			System.out.println(d.toString() + "\n");
		}
	}

	private void callCommandDEL(String command) {
		int id = getCommandID(command);

			FahrzeugVerwaltung dk = (FahrzeugVerwaltung) XMLHandler.getXML(new FahrzeugVerwaltung());
			Fahrzeug d = dk.getFahrzeug(id);
			if (d != null) {
				dk.fahrzeugLoeschen(d.getFahrzeugnummer());
			}
		}
}
