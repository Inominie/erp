package command_handler;

import erp_software.Erp;
import erp_software.XMLHandler;
import interfaces.Command;
import verwaltungs_Objekte.it.Device;
import verwaltungs_Objekte.it.DeviceType;
import verwaltungs_Objekte.it.DeviceVerwaltung;

/**
 * Klasse zur Verwaltung von Devicekommandos <br>
 * Aus dieser Klasse werden die einzelnen Funktionalit�ten der 
 * Klasse {@link verwaltungs_Objekte.it.DeviceVerwaltung} aufgerufen. <br>
 *
 *@see Command
 */
public class DeviceCommands implements Command{

    private String command, singleCommand;

	/**
	 * Commandhandler zum Aufrufen der einzelnen Funktionalitaeten
	 * Der Commandhandler wertet die Eingabe hinsichtlich des Kommandos aus und ruft dann die jeweilige Methode auf.
	 */
    public void startScan() {
        displayConfig("Device");
        command = "";
        singleCommand = "";
        while (!command.equals("BACK")) {
            if (Erp.sc.hasNext()) {
                command = Erp.sc.nextLine();
                singleCommand = command.split("\\s+")[0].toUpperCase();
                command = command.toUpperCase();
                switch (singleCommand) {
                    case "HELP": {
                        displayConfig("Device");
                        break;
                    }
                    case "NEW": {
                        callCommandNEW();
                        break;
                    }
                    case "LSA": {
                        callCommandLSA();
                        break;
                    }
                    case "LS": {
                        callCommandLS(command);
                        break;
                    }
                    case "LSTEC": {
                        callCommandLSTEC(command);
                        break;
                    }
                    case "UP": {
                        callCommandUP(command);
                        break;
                    }
                    case "DEL": {
                        callCommandDEL(command);
                        break;
                    }
                    case "INIT": {
                        XMLHandler.initXMLs(new DeviceVerwaltung());
                        break;
                    }
                    case "BACK": {
                        Erp.startScan();
                        break;
                    }

                }
            }
        }
    }

    
    private void callCommandLSTEC(String command2) {
        int id = getCommandID(command);

        if (id != 0) {
            DeviceVerwaltung ma = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());
            Device m = ma.getDevice(id);
            if (m != null) {
                System.out.println(m.tecToString());
            } else {
                System.out.println("Device mit ID: " + id + " nicht vorhanden!");
            }
        }
    }

    
    //Updatet ein Device
    private void callCommandUP(String command) {
        int id = getCommandID(command);
        if (id != 0) {
            DeviceVerwaltung dk = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());
            Device d = dk.getDevice(id);
            if (d != null) {
                System.out.println("Name eingeben:");
                d.setName(Erp.sc.next());
                System.out.println("Preis eingeben:");
                d.setPreis(Double.parseDouble(Erp.sc.next()));
                System.out.println("Nutzungsdauer eingeben:");
                d.setNutzung(Double.parseDouble(Erp.sc.next()));
                XMLHandler.updateXML(dk);
            } else {
                System.out.println("Device mit ID: " + id + " nicht vorhanden!");
            }
        }
    }

    
    //Fügt ein neues Devices hinzu
    private void callCommandNEW() {
        String name = null, type = null, preis = null, nutzung = null;
        int preise = 0, nutzunge = 0;
        DeviceType typ = null;

        while (name == null || type == null || preis == null || nutzung == null) {
            if (name == null) {
                System.out.println("Bitte Geraetenamen eingeben: ");
                name = Erp.sc.next();
            }
            if (type == null) {
                System.out.println("Bitte Geraetetypen eingeben: ");
                System.out.println("Moegliche Typen sind: \n" + DeviceType.ausgabe());
                type = Erp.sc.next().toUpperCase();
                switch (type) {
                    case "SERVER":
                        typ = DeviceType.SERVER;
                        break;
                    case "WORKSTATION":
                        typ = DeviceType.WORKSTATION;
                        break;
                    case "NOTEBOOK":
                        typ = DeviceType.NOTEBOOK;
                        break;
                    case "HANDY":
                        typ = DeviceType.HANDY;
                        break;
                    case "TABLET":
                        typ = DeviceType.TABLET;
                        break;
                    case "DRUCKER":
                        typ = DeviceType.DRUCKER;
                        break;
                    case "TELEFON":
                        typ = DeviceType.TELEFON;
                        break;
                    case "ROUTER":
                        typ = DeviceType.ROUTER;
                        break;
                    case "SWITCH":
                        typ = DeviceType.SWITCH;
                        break;
                    case "IOTDEVICE":
                        typ = DeviceType.IOTDEVICE;
                        break;
                    default:
                        System.out.println("Der Geraetetyp wird nicht unterstuetzt.");
                }
            }
            if (preis == null) {
                System.out.println("Bitte Anfangspreis angeben: ");
                preis = Erp.sc.next();
                preise = Integer.parseInt(preis);
            }
            if (nutzung == null) {
                System.out.println("Bitte Nutzungsdauer angeben: ");
                nutzung = Erp.sc.next();
                nutzunge = Integer.parseInt(nutzung);
            }
        }
        try {
            DeviceVerwaltung dk = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());
            dk.createDevice(name, typ, preise, nutzunge);
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    
    //Zeigt ein Device an
    private void callCommandLS(String command) {
        int id = getCommandID(command);

        if (id != 0) {
            DeviceVerwaltung ma = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());
            Device m = ma.getDevice(id);
            if (m != null) {
                System.out.println(m.toString());
            } else {
                System.out.println("Device mit ID: " + id + " nicht vorhanden!");
            }
        }
    }

    //Listet alle im System vorhandenen Devices auf
    private void callCommandLSA() {
        DeviceVerwaltung da = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());

        for (Device d : da.getDevices()) {
            System.out.println(d.toString() + "\n");
        }
    }

    
    //Löscht ein Device an Hand der ID
    private void callCommandDEL(String command) {
        int id = getCommandID(command);

            DeviceVerwaltung dk = (DeviceVerwaltung) XMLHandler.getXML(new DeviceVerwaltung());
            Device d = dk.getDevice(id);
            if (d != null) {
                dk.deleteDevice(d);
            }
    }

}