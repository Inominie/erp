package erp_software;
/**
 * Hilfsklasse um ein Paar von generischen Objekten abzuspeichern.
 *
 * @param <K> generisches Objekt
 * @param <V> generisches Objekt
 */
public class Pair<K, V> {

	private final K element0;
	private final V element1;

	/**
	 * Methode zum Anlegen eines neues Pairs
	 * @param element0 erstes Element
	 * @param element1 zweites Element
	 * @return angelegtes Paar
	 */
	public static <K, V> Pair<K, V> createPair(K element0, V element1) {
		return new Pair<K, V>(element0, element1);
	}

	/**
	 * Konstruktor
	 * @param element0 erstes Element
	 * @param element1 zweites Element
	 */
	public Pair(K element0, V element1) {
		this.element0 = element0;
		this.element1 = element1;
	}

	/**
	 * Methode zum Ausgeben des ersten Elemtents
	 * Die Methode liefert als R�ckgabeparameter das erste Element des Pairs
	 * @return erstes Element
	 */
	public K getElement0() {
		return element0;
	}

	/**
	 * Methode zum Ausgeben des zweiten Elements
	 * Die Methode liefert als R�ckgabeparameter das zweite Element des Pairs
	 * @return zweites Element
	 */
	public V getElement1() {
		return element1;
	}

}