/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package erp_software;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vanessa.vogt
 */

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import interfaces.Verwaltung;
import verwaltungs_Objekte.buchungen.Buchung;
import verwaltungs_Objekte.buchungen.BuchungsVerwaltung;
import verwaltungs_Objekte.fleet.Fahrzeug;
import verwaltungs_Objekte.fleet.FahrzeugVerwaltung;
import verwaltungs_Objekte.human_resources.Mitarbeiter;
import verwaltungs_Objekte.human_resources.MitarbeiterVerwaltung;
import verwaltungs_Objekte.it.Device;
import verwaltungs_Objekte.it.DeviceVerwaltung;
import verwaltungs_Objekte.location.Raum;
import verwaltungs_Objekte.location.RaumVerwaltung;
/**
 * Hilfs-Klasse zum Initialisieren / Speichern / Auslesen der <code>XML-Datenbank</code> der einzelnen Verwaltungsklassen.
 * Benutzt die XStream Bibliothek um aus einem Java-Objekt eine XML-Datei zu erstellen oder aus einer XML-Datei ein Java-Objekt.
 * @see <a href="http://x-stream.github.io/" target="_blank">XStream</a>
 */
public class XMLHandler {

    private static XStream xstream = new XStream(new DomDriver());
    /**
     * Methode zum Auslesen einer XML Datei.<br><br>
     * Liest die XML-Datei anhand des Aufrufenden Klassen Namens auf.
     * @param verwaltung Objekt der Verwaltungsklasse dessen Datenbank eingelesen werden soll.
     * @return Objekt der angeforderten Verwaltungsklasse
     */
    public static Object getXML(Verwaltung verwaltung) {
        return xstream.fromXML(Erp.class.getResourceAsStream("/" + verwaltung.getClass().getSimpleName() + ".xml"));
    }
    
    /**
     * Methode zum speichern einer Verwaltungsklasse in Ihrer XML-Datei.
     * @param kartei Objekt einer Verwaltungsklasse
     */
    public static void updateXML(Verwaltung kartei) {

        try {
            xstream.toXML(kartei,
                    new FileOutputStream(Erp.class.getResource("/" + kartei.getClass().getSimpleName() + ".xml").getPath()));
        } catch (IOException ex) {
            Logger.getLogger(XMLHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Methode zur initialen Erstellung der XML-Datein mit Dummy-Eintraegen fuer Verwaltungsobjekten.
     * @param kartei VerwaltungsKlasse die initialisiert werden soll.
     */
    public static void initXMLs(Object kartei) {

        switch (kartei.getClass().getSimpleName()) {
            case "MitarbeiterVerwaltung": {
                MitarbeiterVerwaltung mk = new MitarbeiterVerwaltung();
                mk.getMitarbeiter().add(new Mitarbeiter());
                updateXML(mk);
                break;
            }
            case "RaumVerwaltung": {
                RaumVerwaltung rV = new RaumVerwaltung();
                rV.getRaeume().add(new Raum());
                updateXML(rV);
                break;
            }
            case "DeviceVerwaltung": {
                DeviceVerwaltung dK = new DeviceVerwaltung();
                dK.getDevices().add(new Device());
                updateXML(dK);
                break;
            }
            case "FahrzeugVerwaltung": {
                FahrzeugVerwaltung fV = new FahrzeugVerwaltung();
                fV.getFahrzeuge().add(new Fahrzeug());
                updateXML(fV);
                break;
            }
            case "BuchungsVerwaltung": {
            	BuchungsVerwaltung bV = new BuchungsVerwaltung();
            	bV.getBuchungen().add(new Buchung());
            	updateXML(bV);
            	break;
            }

        }
        System.out.println("Done!");

    }
}
