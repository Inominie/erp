/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package erp_software;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import command_handler.BuchungCommands;
import command_handler.DeviceCommands;
import command_handler.FahrzeugCommands;
import command_handler.MitarbeiterCommands;
import command_handler.RaumCommands;
import verwaltungs_Objekte.buchungen.BuchungsVerwaltung;
import verwaltungs_Objekte.fleet.FahrzeugVerwaltung;
import verwaltungs_Objekte.human_resources.MitarbeiterVerwaltung;
import verwaltungs_Objekte.it.DeviceVerwaltung;
import verwaltungs_Objekte.location.RaumVerwaltung;
/**
 * ERP Software 1.0 -- IFA6A -- Vincent Wack, Vanessa Vogt, Jana Sauerland, Maren Stranghoener<br>
 * Das Programm dient zur Verwaltung von Daten fuer:<br>
 * <ul>
 * <li>{@link verwaltungs_Objekte.human_resources.Mitarbeiter Mitarbeitern}</li>
 * <li>{@link verwaltungs_Objekte.location.Raum Raeume}</li>
 * <li>{@link verwaltungs_Objekte.fleet.Fahrzeug Fahrzeuge}</li>
 * <li>{@link verwaltungs_Objekte.it.Device IT-Geraete}</li>
 * <li>{@link verwaltungs_Objekte.buchungen.Buchung Buchungen}</li>
 * </ul>
 * 
 **/
public class Erp {

    public final static Scanner sc = new Scanner(System.in);

    /**
     * Startmethode des ERP-Systems.<br>
     * Die Startmethode ruft die Hauptmethode des ERP-Systems auf.<br>
     * @param args Standardparameter
     */
    public static void main(String[] args) {
    	
        startScan();
    }

    /**
	 * Commandhandler zum Aufrufen der einzelnen Funktionalitaeten
	 * Der Commandhandler wertet die Eingabe hinsichtlich des Kommandos aus und ruft dann die jeweilige Methode auf.
	 */
    public static void startScan() {
        sysOutConfig();
        String command = "";
        String singleCommand = "";
        while (!command.equals("EXIT")) {
            if (sc.hasNext()) {
                command = sc.nextLine();
                singleCommand = command.split("\\s+")[0].toUpperCase();
                command = command.toUpperCase();
                switch (singleCommand) {
                    case "HELP": {
                        sysOutConfig();
                        break;
                    }
                    case "HR": {
                        new MitarbeiterCommands().startScan();
                        break;
                    }
                    case "IT": {
                        new DeviceCommands().startScan();
                        break;
                    }
                    case "FLEET": {
                        new FahrzeugCommands().startScan();
                        break;
                    }
                    case "ROOM": {
                        new RaumCommands().startScan();
                        break;
                    }
                    case "BUCHUNG": {
                    	new BuchungCommands().startScan();
                    	break;
                    }
                    case "EXIT": {
                        System.out.println("ERP-Software wird beendet.");
                        sc.close();
                        System.exit(0);
                        break;
                    }
                    case "INIT": {
                        XMLHandler.initXMLs(new DeviceVerwaltung());
                        XMLHandler.initXMLs(new MitarbeiterVerwaltung());
                        XMLHandler.initXMLs(new RaumVerwaltung());
                        XMLHandler.initXMLs(new FahrzeugVerwaltung());
                        XMLHandler.initXMLs(new BuchungsVerwaltung());
                        break;
                    }
                    default: {
                        System.out.println("Dieser Befehl wird nicht unterstuetzt!");
                    }
                }
            }
        }
    }

    private static void sysOutConfig() {
        InputStream res = Erp.class.getResourceAsStream("/config");

        BufferedReader reader = new BufferedReader(new InputStreamReader(res));
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
